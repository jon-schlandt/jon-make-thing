import { NavLink } from 'react-router-dom'
import './SiteNav.css'

interface IProps {
  isMobile: boolean
}

export default function SiteNav({ isMobile }: IProps) {
  return (
    <nav className={!isMobile ? 'site-nav' : 'site-nav nav-mobile'}>
      <NavLink
        className='projects-link' 
        to='/projects' 
        activeClassName='nav-selected'
      >
        <div className={!isMobile ? 'portfolio-icon' : 'portfolio-icon icon-mobile'}></div>
        {!isMobile && 'Projects'}
      </NavLink>
      <NavLink
        className='about-link' 
        to='/about' 
        activeClassName='nav-selected'
      >
        <div className={!isMobile ? 'about-icon' : 'about-icon icon-mobile'}></div>
        {!isMobile && 'About'}
      </NavLink>
      {/* <NavLink to='/contact' activeClassName='selected'>Contact</NavLink> */}
    </nav>
  )
}
