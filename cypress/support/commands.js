// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('setUpProjects', () => {
  cy.intercept(
    { method: 'GET', url: `http://localhost:3000/local_data/projects/projects.json`},
    { fixture: `../fixtures/projects.json`}
  )

  cy.visit('http://localhost:3000/projects')
})

Cypress.Commands.add('setUpAbout', () => {
  cy.intercept(
    { method: 'GET', url: `http://localhost:3000/local_data/connections/connections.json`},
    { fixture: `../fixtures/connections.json`}
  )

  cy.visit('http://localhost:3000/about')
})

Cypress.Commands.add('verifyTikTaco', () => {
  cy.get('.project-grid').find('.project').eq(0)
    .find('h3').contains('TikTaco')

  cy.get('.project-grid').find('.project').eq(0)
    .find('.proj-tech')
    .contains('TypeScript | React | React Router | CSS | Cypress')

  cy.get('.project-grid').find('.project').eq(0)
    .find('.proj-desc').children('li').eq(0)
    .contains('Mobile-first application geared towards social media food influencers').next('li')
    .contains('Influencers can quickly and easily generate a unique taco with a random combination of toppings').next()
    .contains('Taco combinations can be favorited for viewing at a later date')
})

Cypress.Commands.add('verifyForeFinder', () => {
  cy.get('.project-grid').find('.project')
    .find('h3').contains('ForeFinder')

  cy.get('.project-grid').find('.project')
    .find('.proj-tech')
    .contains('JavaScript | React | React Router | CSS | Cypress')

  cy.get('.project-grid').find('.project')
    .find('.proj-desc').children('li').eq(0)
    .contains('End-to-end application that makes linking up with others for a round of golf easier').next('li')
    .contains('Private or public tee times can be created, specifying the location, date, and number of golfers').next()
    .contains('Community members can be added as friends to be included in private tee times')
})
