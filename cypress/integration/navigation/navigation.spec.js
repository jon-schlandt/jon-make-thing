describe('Projects navigation link', () => {
  it('should render the Projects view when selected', () => {
    cy.setUpAbout()

    cy.get('.app-header').should('be.visible')
      .find('.projects-link')
      .click()

    cy.url('eq', 'http://localhost:3000/projects')
  })
})

describe('About navigation link', () => {
  it('should render the About view when selected', () => {
    cy.setUpProjects()

    cy.get('.app-header').should('be.visible')
      .find('.about-link')
      .click()
  
    cy.url('eq', 'http://localhost:3000/about')
  })
})
