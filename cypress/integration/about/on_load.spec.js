describe('About view on load', () => {
  beforeEach('set up About', () => {
    cy.setUpAbout()
  })

  it('should display a profile with an introduction message', () => {
    cy.get('.profile').should('be.visible')
      .find('.profile-story>h2')
      .contains('Hi, my name is Jon and I make things.')

    cy.get('.profile')
      .find('.profile-story>p')
      .contains('My work is most fulfilling when I’m helping to improve the lives of others by finding solutions to their day-to-day problems, driving me to build intuitive web applications that focus on providing a valuable experience for the user.')
  })

  it('should display two avenues for viewing my work', () => {
    cy.get('.contact').should('be.visible')
      .find('.connection-grid').eq(0)
      .find('h2')
      .contains('See more things')

    cy.get('.contact')
      .find('.connection-grid').eq(0)
      .find('.connection').eq(0)
      .should('have.attr', 'href', 'https://github.com/jon-schlandt')
      .contains('GitHub')

    cy.get('.contact')
      .find('.connection-grid').eq(0)
      .find('.connection').eq(1)
      .should('have.attr', 'href', 'https://gitlab.com/jon-schlandt')
      .contains('GitLab')
  })

  it('should display two avenues to connect with me', () => {
    cy.get('.contact').should('be.visible')
      .find('.connection-grid').eq(1)
      .find('h2')
      .contains('Say hi')

    cy.get('.contact')
      .find('.connection-grid').eq(1)
      .find('.connection').eq(0)
      .should('have.attr', 'href', 'https://www.linkedin.com/in/jon-schlandt-8479441ab/')
      .contains('LinkedIn')

    cy.get('.contact')
      .find('.connection-grid').eq(1)
      .find('.connection').eq(1)
      .should('have.attr', 'href', 'mailto: schlanjo@gmail.com')
      .contains('Email')
  })

  it('should display a footer with an author\'s note and link to site repo', () => {
    cy.get('.about-footer').should('be.visible')
      .find('.author-msg')
      .contains('Jon Make Thing is built and maintained by Jon (yes, that Jon).')

    cy.get('.about-footer')
      .find('.repo-link>a')
      .should('have.attr', 'href', 'https://gitlab.com/jon-schlandt/jon-make-thing')
      .and('have.attr', 'target', '_blank')
  })
})
