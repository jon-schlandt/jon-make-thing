describe('Projects view on load (mobile)', () => {
  beforeEach(('set up Projects', () => {
    cy.viewport(768, 900)
    cy.setUpProjects()
  }))

  it('should not display display a welcome banner', () => {
    cy.get('.banner-msg').should('not.be.visible')
  })

  it('should display the first project', () => {
    cy.get('.project-grid').should('be.visible')
      .find('.project').should('have.length', 1)

    cy.verifyTikTaco()
  })

  it('should display a footer for selecting projects', () => {
    cy.get('.project-select').should('be.visible')
      .find('.project-btn').should('have.length', 4)
  })
})
