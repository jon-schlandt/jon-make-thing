describe('ProjectSelect button when selected', () => {
  beforeEach('set up Projects view', () => {
    cy.viewport(768, 900)
    cy.setUpProjects()
  })

  it('should render its related project in the Projects view', () => {
    cy.verifyTikTaco()

    cy.get('.project-select').should('be.visible')
      .find('.project-btn').eq(1)
      .click()

    cy.verifyForeFinder()
  })

  it('should not perform any action if already active', () => {
    cy.verifyTikTaco()

    cy.get('.project-select').should('be.visible')
      .find('.project-btn').eq(0)
      .should('have.attr', 'disabled', 'disabled')
      .click({ force: true })

    cy.verifyTikTaco()
  })
})
