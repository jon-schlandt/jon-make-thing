describe('Projects view on load (desktop)', () => {
  beforeEach('set up Projects', () => {
    cy.setUpProjects()
  })

  it('should display a banner with a welcome message', () => {
    cy.get('.banner-msg').should('be.visible')
      .find('h2')
      .contains('Whether on a team or on my own, I love using my creativity to make cool things.')
  })

  it('should display four recent projects', () => {
    cy.get('.project-grid')
      .find('.project').should('have.length', 4)
  })

  it('should not display a footer for selecting projects', () => {
    cy.get('.project-select').should('not.be.visible')
  })
})

describe('Each project on load', () => {
  beforeEach('set up Projects', () => {
    cy.setUpProjects()
  })

  it('should display relevant project info', () => {
    cy.get('.project-grid').should('be.visible').find('.project').eq(0)
      .find('h3')
      .contains('TikTaco')

    cy.verifyTikTaco()
  })

  it('should have one link for viewing its demo and at least one link for viewing its repository', () => {
    cy.get('.project-grid').should('be.visible').find('.project').eq(0)
      .find('.proj-actions').children('a').eq(0)
      .contains('Live Demo')
      .should('have.attr', 'href', 'https://tiktaco.netlify.app')
      .and('have.attr', 'target', '_blank')

    cy.get('.project-grid').find('.project').eq(0)
      .find('.proj-actions').children('a').eq(1)
      .contains('Repo')
      .should('have.attr', 'href', 'https://github.com/jon-schlandt/TikTaco')
  })
})
